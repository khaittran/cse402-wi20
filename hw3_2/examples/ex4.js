function makeURI(ctx, course, year, quarter) {
   const quarterId = quarter.slice(0, 2).toLowerCase();
   return `https://${ctx.domain}/${course}/${year%100}${quarterId}`;
}

function concat(ctx, ...params) {
   return params.join("");
}

const ctx = {
    title: "CSE 402: Introduction to Compiler Construction",
    domain: "www.cs.washington.edu/education/courses",
};

exports.helpers = [
    ["makeURI", makeURI],
    ["concat", concat],
];
exports.blockHelpers = [];
exports.ctx = ctx;
exports.description = "Example 4";
