exports.blockHelpers = [];

function concat(ctx, ...params) {
    return params.join("");
 }

exports.ctx = {};
exports.helpers = [
    ["concat", concat]
];
exports.description = "Simple Parenthesis";