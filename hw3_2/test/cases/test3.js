function each(ctx, a, b) {
    return "" +a +"" + b;
}
 
const ctx = { 
       words: [
           {word: "hi"}
       ]
};

exports.helpers = [
    ["each", each],
];
exports.blockHelpers = [];
exports.ctx = ctx;
exports.description = "helper function named each with each block function";