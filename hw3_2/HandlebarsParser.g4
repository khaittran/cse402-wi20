parser grammar HandlebarsParser;

options { tokenVocab=HandlebarsLexer; }

document : element* EOF ;

element
    :   expressionElement
    | singleBrace 
    | rawElement
    | commentElement
    | block 
    ;

singleBrace : BRACE b=rawElement ;
rawElement  : TEXT ;

block: a=blockStart (body+=element)* b=blockClose ;
blockStart returns [String source, List<String> arguments, String name] : START BLOCK block_helper=helperAllowZero END ;
blockClose returns [String source] : START CLOSE_BLOCK block_helper=ID END ;

expressionElement returns [String source]: START a=smallerElements END ;
smallerElementsNoHelper returns [String source]: a=parenthesis #smallHelperParen
| a=basic #smallHelperBasic
| a=literal #smallHelperLiteral ;
smallerElements returns [String source]: a=parenthesis #smallParen
| a=helperFunction #smallHelper 
| a=basic #smallBasic
| a=literal #smallLiteral ;
basic returns [String sourceS]: a=ID ; 
parenthesis returns [String source]: OPEN_PAREN (exprs+=smallerElements)* CLOSE_PAREN ;
helper returns [String source, List<String> arguments, String name]: function_name=ID args+=smallerElementsNoHelper (args+=smallerElementsNoHelper)* ;
helperAllowZero returns [String source, List<String> Arguments, String name]: function_name=ID (args+=smallerElementsNoHelper)* ;
helperFunction returns [String source]: a=helper ;
literal returns [String source]: FLOAT | INTEGER | STRING ;

commentElement : START COMMENT END_COMMENT ;
