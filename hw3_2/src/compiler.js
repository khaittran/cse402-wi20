const antlr4 = require("antlr4");
const HandlebarsLexer = require("../gen/HandlebarsLexer").HandlebarsLexer;
const HandlebarsParser = require("../gen/HandlebarsParser").HandlebarsParser;
const HandlebarsParserListener = require("../gen/HandlebarsParserListener").HandlebarsParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class HandlebarsCompiler extends HandlebarsParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = [];
        this._usedBlocks = [];

        let eachFunction = function(ctx, body, lst) {
            let result = "";
            let lstVal = `${this._inputVar}`.lst;
            for(let i = 0; i < lst.length; i++) {
                result += body(lst[i]); //is context I am using
            }
            return result;
        };
        let ifFunction = function(ctx, body, expr) {
            if(expr) {
                return body(ctx);
            } else {
                return "";
            }
        };
        let withFunction = function(ctx, body, field) {
            if(ctx.hasOwnProperty(field)){
                return body(ctx[field])
            }
        };
        this.registerBlockHelper("each", eachFunction);
        this.registerBlockHelper("if", ifFunction);
        this.registerBlockHelper("with", withFunction);
    }

    //links a name with function -call with values
    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        let currString =  this._bodyStack.pop(); 
        currString += `return ${this._outputVar};\n`;
        return currString;
    }
    
    compile(template) {
        this._usedHelpers = [];
        this._bodyStack = [];
        this.pushScope();
        const chars = new antlr4.InputStream(template);
        const lexer = new HandlebarsLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new HandlebarsParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this._bodySource = this.popScope();
        let prependFunctions = "";

        for(let i = 0; i < this._usedHelpers.length; i++) {
            let functionDef = "let _$" + this._usedHelpers[i] + " = " + this._helpers.expr[this._usedHelpers[i]].toString() + ";" + "\n";
            prependFunctions += functionDef; //get the function
        }
        let prependBlocks = ""; 
        for(let i = 0; i < this._usedBlocks.length; i++) {
            prependBlocks += "let __$"+ this._usedBlocks[i] + " = " + this._helpers.block[this._usedBlocks[i]].toString() + ";" + "\n";
        }
        this._bodySource = prependFunctions + this._bodySource;
        this._bodySource = prependBlocks + this._bodySource;
        return new Function(this._inputVar, this._bodySource);
    }


    enterBlock(ctx) {
        this.pushScope();
    }

    exitSmallHelperParen(ctx) {
        ctx.source = ctx.a.source;
    }

    exitSmallHelperBasic(ctx) {
        ctx.source = ctx.a.source;
    }

    exitSmallHelperLiteral(ctx) {
        ctx.source = ctx.a.source;
    }

    exitBlockStart(ctx) {
        let functionCall = ctx.block_helper.source;
        let indexParen = functionCall.indexOf("(");
        let name = functionCall.substring(0, indexParen).trim();
        ctx.source = name;
        ctx.name = ctx.block_helper.name;
        if(this._usedBlocks.indexOf(ctx.name) < 0) {
            this._usedBlocks.push(ctx.name);
        }
        ctx.source = ctx.block_helper.source; //naming correct for block
        ctx.arguments = ctx.block_helper.arguments;
    }

    exitBlockClose(ctx) {
        ctx.source = ctx.block_helper.text;
    }

    exitBlock(ctx) {
        let startBlockName = ctx.a.name; //get name 
        let endBlockName = ctx.b.source;
        if(startBlockName != endBlockName) { // error case
           throw("Block start '" + startBlockName + "' does not match the block end '"+ endBlockName +"'.");
        }
        let val = startBlockName;
        if(this._usedBlocks.indexOf(val) < 0) {
            this._usedBlocks.push(val);
        }
        val = "__$" + startBlockName;
        val += "("
        val += this._inputVar; //add in context
        let blockBody = this.popScope(); 
        blockBody = ", function (" + this._inputVar + ") {" + blockBody + "}"
        val += blockBody;
        for(let i = 0; i < ctx.a.arguments.length; i++) { //add in all other variables
            let temp =", " + ctx.a.arguments[i];
            val += temp; 
        }
        val +=")";
        ctx.source = val;
        this.append(ctx.source);
    }

    append(expr) {
        let currString = this._bodyStack.pop(); 
        currString += `${this._outputVar} += ${expr};\n`;
        this._bodyStack.push(currString);
    }

    exitExpressionElement(ctx) {
        //have processed this child already
        this.append(ctx.a.source);
    }

    exitSmallParen(ctx) {
        ctx.source = ctx.a.source;
    }

    exitSmallHelper(ctx) {
        ctx.source = ctx.a.source;
    }

    exitSmallLiteral(ctx) {
        ctx.source = ctx.a.source;
    }

    exitSmallBasic(ctx) {
        ctx.source = ctx.a.source;
    }

    exitLiteral (ctx) {
        ctx.source = ctx.getText();
    }

    exitBasic(ctx) {
        let val = `${this._inputVar}.${ctx.a.text}`;
        ctx.source = val;
    }

    enterSingleBrace(ctx) {
        this.append(`"${escapeString("{")}"`);
    }

    exitParenthesis(ctx) {
        let temp = "";
        for(let i = 0; i < ctx.exprs.length; i++) { //get source values
            temp += ctx.exprs[i].source;
        }
        ctx.source = temp;
    }
    
    exitHelper(ctx) {
        ctx.name = ctx.function_name.text;
        let val = ctx.function_name.text;
        val += "(";
        val += this._inputVar; 
        let functionArguments = [];
        for(let i = 0; i < ctx.args.length; i++) {
            if(ctx.args[i].source != null) {
                let temp =", " + ctx.args[i].source;
                val += temp; 
                functionArguments.push(ctx.args[i].source);
            }
        }
        ctx.arguments = functionArguments;
        val += ")"; //put in parens for beginning and end of expression.
        ctx.source = val; //all the values
    }

    exitHelperAllowZero(ctx) {
        ctx.name = ctx.function_name.text;
        let val = "__$" + ctx.function_name.text;
        val += "(";
        val += this._inputVar; 
        let functionArguments = [];
        for(let i = 0; i < ctx.args.length; i++) {
            if(ctx.args[i].source != null) {
                let temp =", " + ctx.args[i].source;
                val += temp; 
                functionArguments.push(ctx.args[i].source);
            }
        }
        ctx.arguments = functionArguments;
        val += ")"; //put in parens for beginning and end of expression.
        ctx.source = val; //all the values
    }

    exitHelperFunction(ctx) {
        let val = ctx.a.name;
        if(this._usedHelpers.indexOf(val) < 0) {
            this._usedHelpers.push(val);
        }
        ctx.source = "_$" + ctx.a.source;
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }
}

exports.HandlebarsCompiler = HandlebarsCompiler;
